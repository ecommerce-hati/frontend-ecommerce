export interface GetListJenisKain {
  success: boolean;
  data: ListJenisKain[];
  message: string;
}

export interface ListJenisKain {
  id: number;
  jenis_kain: string;
  jenis_proses: string;
  is_fashion: number;
  deskripsi: any;
  fungsi: any;
  created_at: string;
  updated_at: any;
  deleted_at: any;
  created_by: string;
}
