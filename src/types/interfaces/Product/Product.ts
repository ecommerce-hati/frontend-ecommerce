export interface GetListProduct {
  success: boolean;
  data: ListProduct[];
  message: string;
}

export interface ListProduct {
  id: string;
  sddesign_id: number;
  jenis_kain: string;
  jenis_proses: string;
  no_design: string;
  nama_design: string;
  qty_view: number;
  qty_order: number;
  mmbarang_jenis_id: number;
  harga_max: number;
  harga_min: number;
  grade_varian: number;
  warna_varian: number;
  harga: string;
}
