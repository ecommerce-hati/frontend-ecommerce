import { AxiosResponse } from "axios";
import { request } from "./api.service";

// API LOGIN
export const LoginGoogle = async (): Promise<AxiosResponse<any>> => {
  return await request().get("/auth");
};
