import axios from "axios";
import { storageService } from ".";

axios.defaults.xsrfCookieName = "OTHERCOOKIE";
axios.defaults.xsrfHeaderName = "X-OTHERNAME";
axios.defaults.withCredentials = false;
const request = () => {
  return axios.create({
    // baseURL: "https://singular-factual-ostrich.ngrok-free.app/api",
    // baseURL: "http://192.168.0.148:8000/api",
    // baseURL: "http://172.20.10.3:8000/api",
    baseURL: "http://192.168.8.56:8000/api", //ip wifi hati

    timeout: 30000,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
};

const requestProxy = () => {
  const userToken = storageService.getToken();

  let config = {
    headers: {
      //   Authorization: userToken,
      Accept: "application/json",
      "Content-Type": "application/json",
      // token: userToken,
    },
  };

  return {
    get(url: string) {
      return axios.get(url);
    },
    post(url: string, data: any) {
      return axios.post(url, data);
    },
    //   patch(url: string, data: string) {
    //     return axios.patch(
    //       "/api/proxy?url=" + encodeURIComponent(url),
    //       data,
    //       config
    //     );
    //   },
    //   put(url: string, data: string) {
    //     return axios.put(
    //       "/api/proxy?url=" + encodeURIComponent(url),
    //       data,
    //       config
    //     );
    //   },
    //   delete(url: string) {
    //     return axios.delete("/api/proxy?url=" + encodeURIComponent(url), config);
    //   },
  };
};

export { request, requestProxy };
