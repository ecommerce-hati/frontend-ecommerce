import { AxiosResponse } from "axios";
import { request } from "./api.service";

// API CREATE DATA
export const create = async (data: any): Promise<AxiosResponse<any>> => {
  return await request().post("/payments", data);
};

export const getDataTesting = async (): Promise<AxiosResponse<any>> => {
  return await request().get("/payments/testing");
};
