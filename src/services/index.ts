import * as storageService from "./storage.service";
import * as apiService from "./api.service";
import * as paymentService from "./payment.service";
import * as authService from "./auth.service";
import * as productService from "./product.service";

export {
  storageService,
  apiService,
  paymentService,
  authService,
  productService,
};
