import { AxiosResponse } from "axios";
import { request } from "./api.service";
import { GetListJenisKain, GetListProduct } from "@/types";

export const getListProductTop = async (
  tipe: number,
  limit: number
): Promise<AxiosResponse<GetListProduct>> => {
  return await request().get("/produk/filter?tipe=" + tipe + "&limit=" + limit);
};

export const getListProduct = async (
  jenisKategori: string,
  jenisKain: Array<string>,
  jenisProses: Array<string>,
  grade: Array<string>,
  color: string
): Promise<AxiosResponse<GetListProduct>> => {
  return await request().get(
    "/produk/?jenis_kategori=" +
      jenisKategori +
      "&jenis_kain=" +
      jenisKain +
      "&jenis_proses=" +
      jenisProses +
      "&grade=" +
      grade +
      "&color=" +
      color
  );
};

export const getListKain = async (
  isFashion: number | string
): Promise<AxiosResponse<GetListJenisKain>> => {
  return await request().get("/produk/jenis-kain?is_fashion=" + isFashion);
};
