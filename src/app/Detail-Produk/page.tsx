/* eslint-disable @next/next/no-img-element */
"use client";

import Image from "next/image";
import { useState } from "react";
import { FaEye } from "react-icons/fa";
import { IoShirtOutline } from "react-icons/io5";

const DetailProduk = () => {
  const [hargaPerGrade, setHargaPerGrade] = useState<string>(
    "15,000.00 - 20,000.00"
  );
  const [hargaPerGradeConvert, setHargaPerGradeConvert] = useState<number>(0);
  const [hargaAkhir, setHargaAkhir] = useState<number>(0);
  const [selectedGrade, setSelectedGrade] = useState<string>("");
  const [selectedColor, setSelectedColor] = useState<string>("");
  const [selectedQtyPerRoll, setSelectedQtyPerRoll] = useState<number>(0);

  const [selectedDesign, setSelectedDesign] = useState<string>(
    "https://www.bekerjadenganhati.com/masterdesign/public/batik/29.jpg"
  );

  const [activeFloating, setActiveFloating] = useState<boolean>(false);
  const [selectedTypeFloatingUrl, setSelectedTypeFloatingUrl] =
    useState<string>("");
  const [selectedTypeFloating, setSelectedTypeFloating] =
    useState<string>("Kain");

  const [stock, setStock] = useState<number>(15);
  const [quantity, setQuantity] = useState<number>(1);

  const hitungHargaAkhir = async (qtyPerRoll: number, hargaGrade: number) => {
    try {
      let total = hargaGrade * qtyPerRoll;
      setHargaAkhir(total);
    } catch (error) {}
  };

  const hitungHargaAkhirFinal = async (qty: number) => {
    try {
      let total = hargaAkhir * qty;
      setHargaAkhir(total);
    } catch (error) {}
  };

  const formatNumber = (number: number) => {
    return number.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
  };

  const decreaseQuantity = () => {
    if (quantity > 1) {
      let count = quantity - 1;

      setQuantity(count);
      // hitungHargaAkhirFinal(count);
    }
  };

  const increaseQuantity = () => {
    if (quantity < stock) {
      let count = quantity + 1;

      setQuantity(count);
      // hitungHargaAkhirFinal(count);
    }
  };
  return (
    <>
      <div className="bg-zinc-100	">
        {/* <!-- image gallery --> */}
        <section className="container flex-grow mx-auto max-w-[1200px] border-b py-5 lg:grid lg:grid-cols-2 lg:py-10">
          <div className="container mx-auto px-4 w-full">
            <div className=" flex gap-2 w-full mb-2">
              <button
                className={`h-[3vh] items-center md:h-auto px-4 md:px-6 py-4 md:py-0 w-full border border-gray-500  flex justify-center hover:bg-amber-400 duration-100 ${selectedTypeFloating == "Kain" ? "bg-amber-400" : null}`}
                onClick={() => {
                  setSelectedTypeFloating("Kain");
                  setActiveFloating(false);
                }}
              >
                <Image
                  src={"/assets/images/kain-icon.png"}
                  alt=""
                  width={100}
                  height={30}
                  className="w-full"
                />
              </button>
              <button
                className={`h-[3vh] items-center md:h-auto px-4 md:px-6 py-4 md:py-0  w-full border border-gray-500  flex justify-center hover:bg-amber-400 duration-100 ${selectedTypeFloating == "LenganPanjangCowok" ? "bg-amber-400" : null}`}
                onClick={() => {
                  setSelectedTypeFloatingUrl(
                    "https://bekerjadenganhati.com/reyscarf/public/assets/img/model/convert/8/1.png"
                  );
                  setSelectedTypeFloating("LenganPanjangCowok");
                  setActiveFloating(true);
                }}
              >
                <Image
                  src={"/assets/images/shirt-lengan-panjang-icon.png"}
                  alt=""
                  width={100}
                  height={10}
                  className="w-full"
                />
              </button>
              <button
                className={`h-[3vh] items-center md:h-auto px-4 md:px-6 py-4 md:py-0  w-full border border-gray-500  flex justify-center hover:bg-amber-400 duration-100 ${selectedTypeFloating == "Sweater" ? "bg-amber-400" : null}`}
                onClick={() => {
                  setSelectedTypeFloatingUrl(
                    "https://bekerjadenganhati.com/reyscarf/public/assets/img/model/convert/8/2.png"
                  );
                  setSelectedTypeFloating("Sweater");

                  setActiveFloating(true);
                }}
              >
                <Image
                  src={"/assets/images/wanita-lengan-panjang-icon.png"}
                  alt=""
                  width={50}
                  height={10}
                  className="w-full"
                />
              </button>
              <button
                className={`h-[3vh] items-center md:h-auto px-4 md:px-6 py-4 md:py-0  w-full border border-gray-500  flex justify-center hover:bg-amber-400 duration-100 ${selectedTypeFloating == "LenganPendek" ? "bg-amber-400" : null}`}
                onClick={() => {
                  setSelectedTypeFloatingUrl(
                    "https://bekerjadenganhati.com/reyscarf/public/assets/img/model/convert/8/3.png"
                  );
                  setSelectedTypeFloating("LenganPendek");

                  setActiveFloating(true);
                }}
              >
                <Image
                  src={"/assets/images/tshirt-icon.png"}
                  alt=""
                  width={100}
                  height={10}
                  className="w-full"
                />
              </button>
              <button
                className={`h-[3vh] items-center md:h-auto px-4 md:px-6 py-4 md:py-0  w-full border border-gray-500  flex justify-center hover:bg-amber-400 duration-100 ${selectedTypeFloating == "LenganPanjangCewek" ? "bg-amber-400" : null}`}
                onClick={() => {
                  setSelectedTypeFloatingUrl(
                    "https://bekerjadenganhati.com/reyscarf/public/assets/img/model/convert/8/4.png"
                  );
                  setSelectedTypeFloating("LenganPanjangCewek");

                  setActiveFloating(true);
                }}
              >
                <Image
                  src={"/assets/images/baju-celana.png"}
                  alt=""
                  width={100}
                  height={10}
                  className="w-full"
                />
              </button>
              <button
                className={`h-[3vh] items-center md:h-auto px-4 md:px-6 py-4 md:py-0  w-full border border-gray-500  flex justify-center hover:bg-amber-400 duration-100 ${selectedTypeFloating == "Hijab" ? "bg-amber-400" : null}`}
                onClick={() => {
                  setSelectedTypeFloatingUrl(
                    "https://bekerjadenganhati.com/reyscarf/public/assets/img/model/convert/8/5.png"
                  );
                  setSelectedTypeFloating("Hijab");

                  setActiveFloating(true);
                }}
              >
                <Image
                  src={"/assets/images/hijab-icon.png"}
                  alt=""
                  width={100}
                  height={10}
                  className="w-full"
                />
              </button>
            </div>
            {activeFloating ? (
              <div className="h-[90vh]">
                <img
                  className="w-full h-full object-cover object-top"
                  src={selectedTypeFloatingUrl}
                  alt="Sofa image"
                  style={{
                    backgroundSize: "83%",
                    backgroundImage: `url(${selectedDesign})`,
                  }}
                />
              </div>
            ) : (
              <div
                className={`${activeFloating ? "h-[80vh] bg-center bg-no-repeat" : "h-[70vh] bg-cover bg-no-repeat"}`}
                style={{
                  backgroundImage: `url(${selectedDesign})`,
                }}
              ></div>
            )}

            {/* <img
                className="w-full h-full"
                src={selectedDesign}
                alt="Sofa image"
                // style={{
                //   backgroundSize: "83%",
                //   backgroundImage: `url(${selectedDesign})`,
                // }}
              /> */}

            <div className="mt-3 grid grid-cols-4 gap-4">
              <button
                className="h-28 bg-cover"
                style={{
                  backgroundImage:
                    "url('https://www.bekerjadenganhati.com/masterdesign/public/batik/29.jpg')",
                  backgroundPosition: "center",
                  backgroundSize: "cover",
                  backgroundRepeat: "no-repeat",
                }}
                onClick={() => {
                  setSelectedDesign(
                    "https://www.bekerjadenganhati.com/masterdesign/public/batik/29.jpg"
                  );
                  setActiveFloating(false);
                  setSelectedTypeFloating("");
                }}
              >
                {/* Tambahkan elemen img ke dalam div jika perlu */}
              </button>

              <button
                className="h-28 bg-cover"
                style={{
                  backgroundImage:
                    "url('https://www.bekerjadenganhati.com/masterdesign/public/batik/30.jpg')",
                  backgroundPosition: "center",
                  backgroundSize: "cover",
                  backgroundRepeat: "no-repeat",
                }}
                onClick={() => {
                  setSelectedDesign(
                    "https://www.bekerjadenganhati.com/masterdesign/public/batik/30.jpg"
                  );
                  setActiveFloating(false);
                  setSelectedTypeFloating("");
                }}
              >
                {/* Tambahkan elemen img ke dalam div jika perlu */}
              </button>
              <button
                className="h-28 bg-cover"
                style={{
                  backgroundImage:
                    "url('https://www.bekerjadenganhati.com/masterdesign/public/batik/28.jpg')",
                  backgroundPosition: "center",
                  backgroundSize: "cover",
                  backgroundRepeat: "no-repeat",
                }}
                onClick={() => {
                  setSelectedDesign(
                    "https://www.bekerjadenganhati.com/masterdesign/public/batik/28.jpg"
                  );
                  setActiveFloating(false);
                  setSelectedTypeFloating("");
                }}
              >
                {/* Tambahkan elemen img ke dalam div jika perlu */}
              </button>
              {/* <div className="h-28 bg-cover ">
                <img
                  className="cursor-pointer bg-contain h-full"
                  src="https://www.bekerjadenganhati.com/masterdesign/public/batik/28.jpg"
                  alt="kitchen image"
                />
              </div> */}
              <button
                className="h-28 bg-cover"
                style={{
                  backgroundImage:
                    "url('https://www.bekerjadenganhati.com/masterdesign/public/batik/31.jpg')",
                  backgroundPosition: "center",
                  backgroundSize: "cover",
                  backgroundRepeat: "no-repeat",
                }}
                onClick={() => {
                  setSelectedDesign(
                    "https://www.bekerjadenganhati.com/masterdesign/public/batik/31.jpg"
                  );
                  setActiveFloating(false);
                  setSelectedTypeFloating("");
                }}
              >
                {/* Tambahkan elemen img ke dalam div jika perlu */}
              </button>
            </div>
            {/* <!-- /image gallery  --> */}
          </div>

          {/* <!-- description  --> */}

          <div className="mx-auto px-5 lg:px-5 md:w-11/12">
            <h2 className="pt-3 text-2xl font-bold lg:pt-0 uppercase">
              Awesome of Peacock
            </h2>

            <p className="font-bold">
              Kategori: <span className="font-normal">Fashion</span>
            </p>
            <p className="font-bold">
              SKU: <span className="font-normal">12345678</span>
            </p>
            <p className="font-bold">
              Material: <span className="font-normal">Rayon</span>
            </p>

            {/* <p className="pt-5 text-sm leading-5 text-gray-500">
              Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quidem
              exercitationem voluptate sint eius ea assumenda provident eos
              repellendus qui neque! Velit ratione illo maiores voluptates
              commodi eaque illum, laudantium non!
            </p> */}
            <p className="mt-4 text-3xl font-bold text-violet-900">
              IDR {hargaPerGrade} / Yard
              {/* <span className="text-xs text-gray-400 line-through">$550</span> */}
            </p>
            <div className="mt-6">
              <p className="pb-2 text-xs text-gray-500">Grade</p>

              <div className="flex gap-1">
                <button
                  className={`flex h-8 w-8 cursor-pointer items-center justify-center border duration-100 hover:bg-neutral-100 focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500 ${selectedGrade == "A" ? "border-gray-600" : null}`}
                  onClick={() => {
                    setHargaPerGrade("20,000.00");
                    setHargaPerGradeConvert(20000);
                    hitungHargaAkhir(selectedQtyPerRoll, 20000);

                    setSelectedGrade("A");
                  }}
                >
                  A
                </button>
                <button
                  className={`flex h-8 w-8 cursor-pointer items-center justify-center border duration-100 hover:bg-neutral-100 focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500 ${selectedGrade == "B" ? "border-gray-600" : null}`}
                  onClick={() => {
                    setHargaPerGrade("18,000.00");
                    setHargaPerGradeConvert(18000);
                    hitungHargaAkhir(selectedQtyPerRoll, 18000);

                    setSelectedGrade("B");
                  }}
                >
                  B
                </button>
                <button
                  className={`flex h-8 w-8 cursor-pointer items-center justify-center border duration-100 hover:bg-neutral-100 focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500 ${selectedGrade == "BS" ? "border-gray-600" : null}`}
                  onClick={() => {
                    setHargaPerGrade("15,000.00");
                    setHargaPerGradeConvert(15000);
                    hitungHargaAkhir(selectedQtyPerRoll, 15000);

                    setSelectedGrade("BS");
                  }}
                >
                  BS
                </button>
              </div>
            </div>

            <div className="mt-6">
              <p className="pb-2 text-xs text-gray-500">Color</p>

              <div className="flex gap-1">
                <button
                  className={`h-8 w-8 cursor-pointer  bg-[#BDB28C] focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500 ${selectedColor == "BDB28C" ? "border border-gray-600" : null}`}
                  onClick={() => {
                    setSelectedColor("BDB28C");
                    setSelectedDesign(
                      "https://www.bekerjadenganhati.com/masterdesign/public/batik/29.jpg"
                    );
                  }}
                ></button>
                <button
                  className={`h-8 w-8 cursor-pointer  bg-[#8C5A51] focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500 ${selectedColor == "8C5A51" ? "border border-gray-600" : null}`}
                  onClick={() => {
                    setSelectedColor("8C5A51");
                    setSelectedDesign(
                      "https://www.bekerjadenganhati.com/masterdesign/public/batik/30.jpg"
                    );
                  }}
                ></button>
                <button
                  className={`h-8 w-8 cursor-pointer  bg-[#2B6460] focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500 ${selectedColor == "2B6460" ? "border border-gray-600" : null}`}
                  onClick={() => {
                    setSelectedColor("2B6460");
                    setSelectedDesign(
                      "https://www.bekerjadenganhati.com/masterdesign/public/batik/28.jpg"
                    );
                  }}
                ></button>
                <button
                  className={`h-8 w-8 cursor-pointer  bg-[#EBD8A6] focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500 ${selectedColor == "EBD8A6" ? "border border-gray-600" : null}`}
                  onClick={() => {
                    setSelectedColor("EBD8A6");
                    setSelectedDesign(
                      "https://www.bekerjadenganhati.com/masterdesign/public/batik/31.jpg"
                    );
                  }}
                ></button>
              </div>
            </div>

            <div className="mt-6">
              <p className="pb-2 text-xs text-gray-500">QTY / Roll</p>

              <div className="flex gap-1">
                <button
                  className={`flex h-8 w-auto px-4 cursor-pointer items-center justify-center border duration-100 hover:bg-neutral-100 focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500 ${selectedQtyPerRoll == 25 ? "border-gray-600" : null}`}
                  onClick={() => {
                    setSelectedQtyPerRoll(25);
                    hitungHargaAkhir(25, hargaPerGradeConvert);
                  }}
                >
                  25
                </button>
                <button
                  className={`flex h-8 w-auto px-4 cursor-pointer items-center justify-center border duration-100 hover:bg-neutral-100 focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500 ${selectedQtyPerRoll == 50 ? "border-gray-600" : null}`}
                  onClick={() => {
                    setSelectedQtyPerRoll(50);
                    hitungHargaAkhir(50, hargaPerGradeConvert);
                  }}
                >
                  50
                </button>
                <button
                  className={`flex h-8 w-auto px-4 cursor-pointer items-center justify-center border duration-100 hover:bg-neutral-100 focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500 ${selectedQtyPerRoll == 100 ? "border-gray-600" : null}`}
                  onClick={() => {
                    setSelectedQtyPerRoll(100);
                    hitungHargaAkhir(100, hargaPerGradeConvert);
                  }}
                >
                  100
                </button>
              </div>
            </div>

            <div className="mt-6">
              <p className="pb-2 text-xs text-gray-500">Quantity</p>

              <div className="flex">
                <button
                  className="flex h-8 w-8 cursor-pointer items-center justify-center border duration-100 hover:bg-neutral-100 focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500"
                  onClick={decreaseQuantity}
                >
                  &minus;
                </button>
                <div className="flex h-8 w-8 cursor-text items-center justify-center border-t border-b active:ring-gray-500">
                  {quantity}
                </div>
                <button
                  className="flex h-8 w-8 cursor-pointer items-center justify-center border duration-100 hover:bg-neutral-100 focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500"
                  onClick={increaseQuantity}
                >
                  &#43;
                </button>
                <div className="flex justify-center items-center px-2">
                  Stok Total : &nbsp; <b> {stock}</b>
                </div>
              </div>
            </div>
            <p className="mt-4 text-3xl font-bold text-violet-900">
              IDR {formatNumber(hargaAkhir)} / Roll
              {/* <span className="text-xs text-gray-400 line-through">$550</span> */}
            </p>

            <div className="mt-7 flex flex-row items-center md:gap-6  w-full ">
              <button className="flex h-12 w-3/4 px-2 md:px-6 items-center justify-center bg-violet-900 text-white duration-100 hover:bg-blue-800 text-sm md:text-base">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke-width="1.5"
                  stroke="currentColor"
                  className="mr-2 h-4 w-4"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z"
                  />
                </svg>
                Masukkan Keranjang
              </button>
              <button className="flex h-12 w-2/4 items-center justify-center bg-amber-400 duration-100 hover:bg-yellow-300 text-sm md:text-base">
                {/* <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke-width="1.5"
                  stroke="currentColor"
                  className="mr-3 h-4 w-4"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
                  />
                </svg> */}
                Beli Sekarang
              </button>
            </div>
          </div>
        </section>
        <section className="container mx-auto max-w-[1200px] px-5 py-5 lg:py-10 ">
          <h2 className="text-xl">Product details</h2>
          <p className="mt-4 lg:w-3/4">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta
            consequatur temporibus deserunt id labore. Et, iusto nostrum
            repellat laudantium iure fuga quibusdam laborum laboriosam earum.
            Fugit possimus impedit harum dolor? <br />
            Laboriosam quo impedit, reprehenderit eum eaque eius tempore non
            blanditiis, labore quibusdam nesciunt atque doloribus cum autem?
            <br />
            Autem magni ullam alias pariatur corporis officiis animi neque, quo,
            ab aperiam ratione! Similique deserunt dolore dignissimos, iure
            quisquam mollitia perferendis pariatur reprehenderit dolorem, cum
            enim aut ad amet in ducimus sint, commodi neque quis saepe libero
            dolor dolores. Sequi voluptas adipisci minus!
          </p>

          <table className="mt-7 w-full table-auto divide-x divide-y lg:w-1/2">
            <tbody className="divide-x border">
              <tr>
                <td className="border pl-4 font-bold">Color</td>
                <td className="border pl-4">Black, Brown, Red</td>
              </tr>

              <tr>
                <td className="border pl-4 font-bold">Material</td>
                <td className="border pl-4">Latex</td>
              </tr>

              <tr>
                <td className="border pl-4 font-bold">Weight</td>
                <td className="border pl-4">55 Kg</td>
              </tr>
            </tbody>
          </table>
        </section>

        <p className="mx-auto mt-10 mb-5 max-w-[1200px] px-5 uppercase">
          Rekomendasi Lainnya
        </p>
        <div className="mx-auto grid max-w-[1200px] grid-cols-2 gap-3 px-5 pb-10 lg:grid-cols-4">
          <div className="flex flex-col sm:h-auto sm:max-h-[1200px]  justify-center  px-2 ">
            <div className="border border-gray-300 pb-4 rounded-xl shadow-md">
              <div className="h-2/4 ">
                <a href="Detail-Produk" className="">
                  <div className="relative flex">
                    <div className="h-full md:h-full ">
                      {/* <img
                    className=""
                    src="./assets/images/product-chair.png"
                    alt="sofa image"
                  /> */}
                      <Image
                        src={"/assets/images/product-chair.png"}
                        width={300}
                        height={0}
                        className="h-full rounded-t-xl"
                        alt=""
                      />
                    </div>
                    <div className="absolute flex h-full w-full items-center justify-center gap-3 opacity-0 duration-150 hover:opacity-100">
                      <span className="flex h-8 w-8 cursor-pointer items-center justify-center rounded-full bg-amber-400">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth="1.5"
                          stroke="currentColor"
                          className="h-4 w-4"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                          />
                        </svg>
                      </span>
                    </div>
                  </div>

                  <div>
                    <div className="bg-gradient-to-r from-violet-900 to-purple-500  py-1 px-2 rounded-br-full flex justify-between items-center">
                      <p className="uppercase text-white font-medium text-sm md:text-base ">
                        <IoShirtOutline />
                      </p>
                      <p className="uppercase text-white font-medium text-sm md:text-base px-4">
                        #Rayon
                      </p>
                    </div>
                    <div className="px-2">
                      <div className="mt-2 flex justify-between gap-2 ">
                        <p className="uppercase text-sm md:text-base w-2/4 md:w-3/4 ">
                          123456789 tessss
                        </p>
                      </div>
                      <div className="flex justify-between my-1">
                        <p className="text-xs md:text-sm">
                          3 Varian Warna | 3 Varian Grade
                        </p>
                        {/* <p className="text-xs md:text-sm bg-red-400">
                    </p> */}
                      </div>

                      <p className=" font-semibold text-base  text-violet-900 my-2">
                        IDR 120.000,00 - 180.000,00 / Yard
                      </p>

                      <p className="text-xs md:text-sm text-gray-500 flex items-center">
                        <FaEye className="mr-2 text-base" /> 200 Dilihat | 100
                        Terjual
                      </p>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <div className="flex flex-col sm:h-auto sm:max-h-[1200px]  justify-center  px-2">
            <div className="border border-gray-300 pb-4 rounded-xl shadow-md">
              <div className="h-2/4 ">
                <a href="Detail-Produk" className="">
                  <div className="relative flex">
                    <div className="h-full md:h-full ">
                      {/* <img
                    className=""
                    src="./assets/images/product-chair.png"
                    alt="sofa image"
                  /> */}
                      <Image
                        src={"/assets/images/product-chair.png"}
                        width={300}
                        height={0}
                        className="h-full rounded-t-xl"
                        alt=""
                      />
                    </div>
                    <div className="absolute flex h-full w-full items-center justify-center gap-3 opacity-0 duration-150 hover:opacity-100">
                      <span className="flex h-8 w-8 cursor-pointer items-center justify-center rounded-full bg-amber-400">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth="1.5"
                          stroke="currentColor"
                          className="h-4 w-4"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                          />
                        </svg>
                      </span>
                    </div>
                  </div>

                  <div>
                    <div className="bg-gradient-to-r from-violet-900 to-purple-500  py-1 px-2 rounded-br-full flex justify-between items-center">
                      <p className="uppercase text-white font-medium text-sm md:text-base ">
                        <IoShirtOutline />
                      </p>
                      <p className="uppercase text-white font-medium text-sm md:text-base px-4">
                        #Rayon
                      </p>
                    </div>
                    <div className="px-2">
                      <div className="mt-2 flex justify-between gap-2 ">
                        <p className="uppercase text-sm md:text-base w-2/4 md:w-3/4 ">
                          123456789 tessss
                        </p>
                      </div>
                      <div className="flex justify-between my-1">
                        <p className="text-xs md:text-sm">
                          3 Varian Warna | 3 Varian Grade
                        </p>
                        {/* <p className="text-xs md:text-sm bg-red-400">
                    </p> */}
                      </div>

                      <p className=" font-semibold text-base  text-violet-900 my-2">
                        IDR 120.000,00 - 180.000,00 / Yard
                      </p>

                      <p className="text-xs md:text-sm text-gray-500 flex items-center">
                        <FaEye className="mr-2 text-base" /> 200 Dilihat | 100
                        Terjual
                      </p>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <div className="flex flex-col sm:h-auto sm:max-h-[1200px]  justify-center  px-2">
            <div className="border border-gray-300 pb-4 rounded-xl shadow-md">
              <div className="h-2/4 ">
                <a href="Detail-Produk" className="">
                  <div className="relative flex">
                    <div className="h-full md:h-full ">
                      {/* <img
                    className=""
                    src="./assets/images/product-chair.png"
                    alt="sofa image"
                  /> */}
                      <Image
                        src={"/assets/images/product-chair.png"}
                        width={300}
                        height={0}
                        className="h-full rounded-t-xl"
                        alt=""
                      />
                    </div>
                    <div className="absolute flex h-full w-full items-center justify-center gap-3 opacity-0 duration-150 hover:opacity-100">
                      <span className="flex h-8 w-8 cursor-pointer items-center justify-center rounded-full bg-amber-400">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth="1.5"
                          stroke="currentColor"
                          className="h-4 w-4"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                          />
                        </svg>
                      </span>
                    </div>
                  </div>

                  <div>
                    <div className="bg-gradient-to-r from-violet-900 to-purple-500  py-1 px-2 rounded-br-full flex justify-between items-center">
                      <p className="uppercase text-white font-medium text-sm md:text-base ">
                        <IoShirtOutline />
                      </p>
                      <p className="uppercase text-white font-medium text-sm md:text-base px-4">
                        #Rayon
                      </p>
                    </div>
                    <div className="px-2">
                      <div className="mt-2 flex justify-between gap-2 ">
                        <p className="uppercase text-sm md:text-base w-2/4 md:w-3/4 ">
                          123456789 tessss
                        </p>
                      </div>
                      <div className="flex justify-between my-1">
                        <p className="text-xs md:text-sm">
                          3 Varian Warna | 3 Varian Grade
                        </p>
                        {/* <p className="text-xs md:text-sm bg-red-400">
                    </p> */}
                      </div>

                      <p className=" font-semibold text-base  text-violet-900 my-2">
                        IDR 120.000,00 - 180.000,00 / Yard
                      </p>

                      <p className="text-xs md:text-sm text-gray-500 flex items-center">
                        <FaEye className="mr-2 text-base" /> 200 Dilihat | 100
                        Terjual
                      </p>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <div className="flex flex-col sm:h-auto sm:max-h-[1200px]  justify-center  px-2">
            <div className="border border-gray-300 pb-4 rounded-xl shadow-md">
              <div className="h-2/4 ">
                <a href="Detail-Produk" className="">
                  <div className="relative flex">
                    <div className="h-full md:h-full ">
                      {/* <img
                    className=""
                    src="./assets/images/product-chair.png"
                    alt="sofa image"
                  /> */}
                      <Image
                        src={"/assets/images/product-chair.png"}
                        width={300}
                        height={0}
                        className="h-full rounded-t-xl"
                        alt=""
                      />
                    </div>
                    <div className="absolute flex h-full w-full items-center justify-center gap-3 opacity-0 duration-150 hover:opacity-100">
                      <span className="flex h-8 w-8 cursor-pointer items-center justify-center rounded-full bg-amber-400">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth="1.5"
                          stroke="currentColor"
                          className="h-4 w-4"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                          />
                        </svg>
                      </span>
                    </div>
                  </div>

                  <div>
                    <div className="bg-gradient-to-r from-violet-900 to-purple-500  py-1 px-2 rounded-br-full flex justify-between items-center">
                      <p className="uppercase text-white font-medium text-sm md:text-base ">
                        <IoShirtOutline />
                      </p>
                      <p className="uppercase text-white font-medium text-sm md:text-base px-4">
                        #Rayon
                      </p>
                    </div>
                    <div className="px-2">
                      <div className="mt-2 flex justify-between gap-2 ">
                        <p className="uppercase text-sm md:text-base w-2/4 md:w-3/4 ">
                          123456789 tessss
                        </p>
                      </div>
                      <div className="flex justify-between my-1">
                        <p className="text-xs md:text-sm">
                          3 Varian Warna | 3 Varian Grade
                        </p>
                        {/* <p className="text-xs md:text-sm bg-red-400">
                    </p> */}
                      </div>

                      <p className=" font-semibold text-base  text-violet-900 my-2">
                        IDR 120.000,00 - 180.000,00 / Yard
                      </p>

                      <p className="text-xs md:text-sm text-gray-500 flex items-center">
                        <FaEye className="mr-2 text-base" /> 200 Dilihat | 100
                        Terjual
                      </p>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DetailProduk;
