export const ConvertNamaProduk = (value: string) => {
  return value.substring(15).replace(/_/g, " ");
};
