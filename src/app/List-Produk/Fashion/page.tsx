/* eslint-disable @next/next/no-img-element */
"use client";

import { ConvertNamaProduk } from "@/app/Utils";
import { productService } from "@/services";
import { ListJenisKain, ListProduct } from "@/types";
import Image from "next/image";
import { useEffect, useState } from "react";
import { FaEye } from "react-icons/fa";
import { IoShirtOutline } from "react-icons/io5";

const ListProdukFashion = () => {
  const [listProduk, setListProduk] = useState<Array<ListProduct>>([]);
  const [jenisKategori, setJenisKategori] = useState<string>("FASHION");
  const [jenisKain, setJenisKain] = useState<Array<string>>([""]);
  const [jenisProses, setJenisProses] = useState<Array<string>>([""]);
  const [grade, setGrade] = useState<Array<string>>([""]);
  const [color, setColor] = useState<string>("");
  const [visibleProducts, setVisibleProducts] = useState(6);
  const [listJenisKain, setListJenisKain] = useState<Array<ListJenisKain>>([]);
  const [jenisKainChoice, setJenisKainChoice] = useState<Array<any>>([]);
  const [jenisProsesChoice, setJenisProsesChoice] = useState<Array<any>>([]);

  const loadMore = () => {
    setVisibleProducts((prev) => prev + 6);
  };

  const getListProduk = async () => {
    try {
      const response = await productService.getListProduct(
        jenisKategori,
        jenisKainChoice,
        jenisProsesChoice,
        grade,
        color
      );
      setListProduk(response?.data?.data);
      // console.log(response.data.data);
    } catch (error) {}
  };

  const getJenisKain = async () => {
    try {
      const response = await productService.getListKain("1");

      setListJenisKain(response?.data?.data);
      // console.log(response.data.data);
    } catch (error) {}
  };

  function capitalize(str: any) {
    return str.toLowerCase().replace(/(?:^|\s)\S/g, function (a: string) {
      return a.toUpperCase();
    });
  }

  let arr = [...jenisKainChoice];
  let arrProses = [...jenisProsesChoice];
  const pilihJenisKain = async (e: any) => {
    try {
      let updatedJenisKainChoice;
      if (jenisKainChoice.includes(e)) {
        // Jika jenis kain sudah dipilih, hilangkan dari array
        updatedJenisKainChoice = jenisKainChoice.filter((item) => item !== e);
      } else {
        // Jika belum dipilih, tambahkan ke array
        updatedJenisKainChoice = [...jenisKainChoice, e];
      }

      setJenisKainChoice(updatedJenisKainChoice);

      if (
        JSON.stringify(updatedJenisKainChoice) !==
          JSON.stringify(jenisKainChoice) ||
        JSON.stringify(jenisProsesChoice) !== JSON.stringify(jenisProsesChoice)
      ) {
        // Perbarui list produk hanya jika ada perubahan pada jenis kain atau jenis proses
        const response = await productService.getListProduct(
          jenisKategori,
          updatedJenisKainChoice,
          jenisProsesChoice,
          grade,
          color
        );
        setListProduk(response?.data?.data);
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  const pilihJenisProses = async (e: any) => {
    try {
      let updatedJenisProsesChoice;
      if (jenisProsesChoice.includes(e)) {
        // Jika jenis proses sudah dipilih, hilangkan dari array
        updatedJenisProsesChoice = jenisProsesChoice.filter(
          (item) => item !== e
        );
      } else {
        // Jika belum dipilih, tambahkan ke array
        updatedJenisProsesChoice = [...jenisProsesChoice, e];
      }

      setJenisProsesChoice(updatedJenisProsesChoice);

      if (
        JSON.stringify(jenisKainChoice) !== JSON.stringify(jenisKainChoice) ||
        JSON.stringify(updatedJenisProsesChoice) !==
          JSON.stringify(jenisProsesChoice)
      ) {
        // Perbarui list produk hanya jika ada perubahan pada jenis kain atau jenis proses
        const response = await productService.getListProduct(
          jenisKategori,
          jenisKainChoice,
          updatedJenisProsesChoice,
          grade,
          color
        );
        setListProduk(response?.data?.data);
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  useEffect(() => {
    // Lakukan pengambilan data jenis kain hanya saat komponen pertama kali dimuat
    getJenisKain();
  }, []);

  useEffect(() => {
    // Perbarui list produk saat terjadi perubahan pada jenis kain atau proses
    getListProduk();
  }, [jenisKainChoice, jenisProsesChoice]);

  return (
    <>
      <div className="relative">
        <img
          className="w-full object-cover brightness-50 filter lg:h-[500px]"
          src="/assets/images/header-bg.jpeg"
          alt="Living room image"
        />

        <div className="absolute top-1/2 left-1/2 mx-auto flex w-11/12 max-w-[1200px] -translate-x-1/2 -translate-y-1/2 flex-col text-center text-white lg:ml-5">
          <h1 className="text-4xl font-bold sm:text-5xl lg:text-left">
            Fashion
          </h1>
          <p className="pt-3 text-xs lg:w-3/5 lg:pt-5 lg:text-left lg:text-base">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit.
            Consequatur aperiam natus, nulla, obcaecati nesciunt, itaque
            adipisci earum ducimus pariatur eaque labore.
          </p>
          <button className="mx-auto mt-5 w-1/2 bg-amber-400 px-3 py-1 text-black duration-100 hover:bg-yellow-300 lg:mx-0 lg:h-10 lg:w-2/12 lg:px-10">
            Order Now
          </button>
        </div>
      </div>

      <section className="container mx-auto flex-grow max-w-[1200px] border-b py-5 lg:flex lg:flex-row lg:py-10 ">
        {/* <!-- sidebar  --> */}
        <section className="hidden w-[300px] flex-shrink-0 px-4 lg:block">
          <div className="sticky top-6">
            <div className="flex border-b pb-5 ">
              <div className="w-full">
                <p className="mb-3 font-medium uppercase">Materials</p>
                <div className="h-[50vh] overflow-y-scroll">
                  {listJenisKain.map((list, index) => (
                    <div
                      className="flex w-full justify-between gap-2"
                      key={index}
                    >
                      <div className="flex justify-center items-center ">
                        <input
                          type="checkbox"
                          id={`jenis_kain_${index}`}
                          onClick={(e) => pilihJenisKain(list.jenis_kain)}

                          // className="outline-none"
                        />
                        <label
                          htmlFor={`jenis_kain_${index}`}
                          className="ml-4 capitalize"
                        >
                          {capitalize(list.jenis_kain)}
                        </label>
                      </div>
                      <div>
                        <p className="text-gray-500">(12)</p>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>

            <div className="flex border-b py-5">
              <div className="w-full">
                <p className="mb-3 font-medium uppercase">Proses</p>

                <div className="flex w-full justify-between">
                  <div className="flex justify-center items-center">
                    <input
                      type="checkbox"
                      onClick={() => pilihJenisProses("Polosan")}
                      id="polosan"
                    />
                    <label className="ml-4" htmlFor="polosan">
                      Polosan
                    </label>
                  </div>
                  <div>
                    <p className="text-gray-500">(12)</p>
                  </div>
                </div>

                <div className="flex w-full justify-between">
                  <div className="flex justify-center items-center">
                    <input
                      type="checkbox"
                      onClick={() => pilihJenisProses("Printing")}
                      id="printing"
                    />
                    <label className="ml-4" htmlFor="printing">
                      Printing
                    </label>
                  </div>
                  <div>
                    <p className="text-gray-500">(15)</p>
                  </div>
                </div>

                <div className="flex w-full justify-between">
                  <div className="flex justify-center items-center">
                    <input
                      type="checkbox"
                      onClick={() => pilihJenisProses("Digital Print")}
                      id="digital-print"
                    />
                    <label className="ml-4" htmlFor="digital-print">
                      Digital Print
                    </label>
                  </div>
                  <div>
                    <p className="text-gray-500">(14)</p>
                  </div>
                </div>
              </div>
            </div>

            <div className="flex border-b py-5">
              <div className="w-full">
                <p className="mb-3 font-medium">Grade</p>

                <div className="flex gap-2">
                  <div className="flex h-8 w-8 cursor-pointer items-center justify-center border duration-100 hover:bg-neutral-100 focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500">
                    A
                  </div>
                  <div className="flex h-8 w-8 cursor-pointer items-center justify-center border duration-100 hover:bg-neutral-100 focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500">
                    B
                  </div>
                  <div className="flex h-8 w-8 cursor-pointer items-center justify-center border duration-100 hover:bg-neutral-100 focus:ring-2 focus:ring-gray-500 active:ring-2 active:ring-gray-500">
                    BS
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* <!-- /sidebar  --> */}

        <div>
          <div className="mb-5 flex items-center justify-between px-5">
            <div className="flex gap-3">
              <button className="flex items-center justify-center border px-6 py-2">
                Sort by
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="mx-2 h-4 w-4"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M19.5 8.25l-7.5 7.5-7.5-7.5"
                  />
                </svg>
              </button>

              <button className="flex items-center justify-center border px-6 py-2 md:hidden">
                Filters
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="mx-2 h-4 w-4"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M19.5 8.25l-7.5 7.5-7.5-7.5"
                  />
                </svg>
              </button>
            </div>

            <div className="hidden gap-3 lg:flex">
              <button className="border bg-amber-400 py-2 px-2">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="h-5 w-5"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M3.75 6A2.25 2.25 0 016 3.75h2.25A2.25 2.25 0 0110.5 6v2.25a2.25 2.25 0 01-2.25 2.25H6a2.25 2.25 0 01-2.25-2.25V6zM3.75 15.75A2.25 2.25 0 016 13.5h2.25a2.25 2.25 0 012.25 2.25V18a2.25 2.25 0 01-2.25 2.25H6A2.25 2.25 0 013.75 18v-2.25zM13.5 6a2.25 2.25 0 012.25-2.25H18A2.25 2.25 0 0120.25 6v2.25A2.25 2.25 0 0118 10.5h-2.25a2.25 2.25 0 01-2.25-2.25V6zM13.5 15.75a2.25 2.25 0 012.25-2.25H18a2.25 2.25 0 012.25 2.25V18A2.25 2.25 0 0118 20.25h-2.25A2.25 2.25 0 0113.5 18v-2.25z"
                  />
                </svg>
              </button>

              <button className="border py-2 px-2">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="h-5 w-5"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M8.25 6.75h12M8.25 12h12m-12 5.25h12M3.75 6.75h.007v.008H3.75V6.75zm.375 0a.375.375 0 11-.75 0 .375.375 0 01.75 0zM3.75 12h.007v.008H3.75V12zm.375 0a.375.375 0 11-.75 0 .375.375 0 01.75 0zm-.375 5.25h.007v.008H3.75v-.008zm.375 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z"
                  />
                </svg>
              </button>
            </div>
          </div>

          <section className="mx-auto grid max-w-[1200px] grid-cols-2 gap-3 px-5 pb-10 lg:grid-cols-3">
            {listProduk?.slice(0, visibleProducts).map((list, index) => (
              <div
                className="flex flex-col sm:h-auto sm:max-h-[1200px] justify-center  px-2"
                key={index}
              >
                <div className="border border-gray-300 pb-4 rounded-xl h-full">
                  <div className="h-2/4 ">
                    <a href="Detail-Produk" className="">
                      <div className="relative flex">
                        <div className="h-full md:h-full ">
                          <Image
                            src={"/assets/images/product-chair.png"}
                            width={300}
                            height={0}
                            className="h-full rounded-t-xl"
                            alt=""
                          />
                        </div>
                        <div className="absolute flex h-full w-full items-center justify-center gap-3 opacity-0 duration-150 hover:opacity-100">
                          <span className="flex h-8 w-8 cursor-pointer items-center justify-center rounded-full bg-amber-400">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              fill="none"
                              viewBox="0 0 24 24"
                              strokeWidth="1.5"
                              stroke="currentColor"
                              className="h-4 w-4"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                              />
                            </svg>
                          </span>
                        </div>
                      </div>

                      <div>
                        <div className="bg-gradient-to-r from-violet-900 to-purple-500  py-1 px-2 rounded-br-full flex justify-between items-center">
                          <p className="uppercase text-white font-medium text-sm md:text-base ">
                            <IoShirtOutline />
                          </p>
                          <p className="uppercase text-white font-medium text-xs md:text-sm px-4">
                            #{list?.jenis_kain}
                          </p>
                        </div>
                        <div className="px-2">
                          <div className="mt-2 flex justify-between gap-2 ">
                            <p className="uppercase text-sm md:text-base w-2/4 md:w-full ">
                              {ConvertNamaProduk(list.nama_design)}
                            </p>
                          </div>
                          <div className="flex justify-between my-1">
                            <p className="text-xs md:text-sm">
                              {list?.warna_varian} Varian Warna |{" "}
                              {list?.grade_varian} Varian Grade
                            </p>
                          </div>
                          <p className=" font-semibold text-base  text-violet-900 my-2">
                            IDR {list?.harga} / Yard
                          </p>

                          <p className="text-xs md:text-sm text-gray-500 flex items-center">
                            <FaEye className="mr-2 text-base" />{" "}
                            {list?.qty_view} Dilihat | {list?.qty_order} Terjual
                          </p>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            ))}
          </section>
          {visibleProducts < listProduk?.length && (
            <div className="flex justify-center mt-4">
              <button
                onClick={loadMore}
                className="bg-amber-400 hover:bg-amber-600 text-white font-bold py-2 px-4 rounded"
              >
                Lainnya
              </button>
            </div>
          )}
          {/* <!-- 2 --> */}
        </div>
      </section>
    </>
  );
};

export default ListProdukFashion;
