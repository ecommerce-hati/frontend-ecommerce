interface TabHeaderCheckoutProps {
  tabActive: string;
}

const TabHeaderCheckout: React.FC<TabHeaderCheckoutProps> = ({
  tabActive = "",
}) => {
  return (
    <table className="hidden lg:table">
      <thead className="h-16 bg-neutral-100">
        <tr>
          <th
            className={`${tabActive == "alamat" ? "bg-neutral-600 text-white" : null}`}
          >
            1. ALAMAT
          </th>
          <th
            className={`${tabActive == "metode-pengiriman" ? "bg-neutral-600 text-white" : null}`}
          >
            2. METODE PENGIRIMAN
          </th>
          <th
            className={`${tabActive == "tinjau-pesanan" ? "bg-neutral-600 text-white" : null}`}
          >
            3. TINJAU PESANAN
          </th>
        </tr>
      </thead>
    </table>
  );
};

export default TabHeaderCheckout;
