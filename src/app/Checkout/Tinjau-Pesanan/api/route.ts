import { NextResponse } from "next/server";

// export async function GET() {
//   const res = await fetch(
//     "https://f0e0-103-151-214-181.ngrok-free.app/api/payments/testing",
//     {
//       headers: {
//         "Content-Type": "application/json",
//       },

//       next: { revalidate: 60 }, // Revalidate every 60 seconds
//     }
//   );
//   const data = await res.json();
//   // console.log({data});

//   return NextResponse.json(data);
// }

export async function GET() {
  return new Response("Hello World!");
}
