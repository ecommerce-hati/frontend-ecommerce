"use client";
import React from "react";

import TabHeaderCheckout from "@/app/component/TabHeaderCheckout/TabHeaderCheckout";
import { paymentService } from "@/services";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

/* eslint-disable @next/next/no-img-element */

export interface RequestCheckout {
  description: string;
  amount: number;
  items: Item[];
}

export interface Item {
  name: string;
  quantity: number;
  price: number;
}

const TinjauPesanan = () => {
  const [testing, setTesting] = useState<any>();
  const [itemCheckout, setItemCheckout] = useState<any>();
  const [totalPrice, setTotalPrice] = useState(0); // State baru untuk menyimpan total harga
  // const router = useRouter();

  let data = [
    {
      name: "ITALIAN BED",
      quantity: 1,
      qtyPerRoll: 25,
      price: 15000,
      totalPrice: 15000,
    },
    {
      name: "SPAIN BED",
      quantity: 2,
      qtyPerRoll: 25,
      price: 40000,
      totalPrice: 80000,
    },
  ];
  const getData = async () => {
    try {
      // const data = await paymentService.getDataTesting();
      // const res = await axios.get<any>("/tinjau/api");

      // console.log({ data });
      setItemCheckout(data);
    } catch (error) {}
  };

  // Fungsi untuk menghitung total harga dari data
  const calculateTotalPrice = (data: any) => {
    const totalPrice = data.reduce(
      (acc: number, item: { price: number; quantity: number }) =>
        acc + item.price * item.quantity,
      0
    );

    // console.log({ totalPrice });
    setTotalPrice(totalPrice);
  };

  const actionCheckout = async () => {
    try {
      let dataRequest: RequestCheckout;

      dataRequest = {
        description: "Bursa Kain",
        amount: totalPrice,
        items: data,
      };

      const response = await paymentService.create(dataRequest);
      // console.log("resp", response);

      if (response.data) {
        window.open(response.data.checkout_link, "_blank");
      }
    } catch (error) {}
  };

  useEffect(() => {
    getData();
    calculateTotalPrice(data);
  }, []);
  return (
    <div className="flex-grow">
      <section className="container mx-auto max-w-[1200px] py-5 lg:flex lg:flex-row lg:py-10">
        <h2 className="mx-auto px-5 text-2xl font-bold md:hidden">
          Checkout Review
        </h2>
        {/* <!-- form  --> */}
        <section className="grid w-full max-w-[1200px] grid-cols-1 gap-3 px-5 pb-10">
          {/* <table className="hidden lg:table">
            <thead className="h-16 bg-neutral-100">
              <tr>
                <th>ADDRESS</th>
                <th>DELIVERY METHOD</th>
                <th>PAYMENT METHOD</th>
                <th className="bg-neutral-600 text-white">ORDER REVIEW</th>
              </tr>
            </thead>
          </table> */}

          <TabHeaderCheckout tabActive="tinjau-pesanan" />

          {/* <!-- Mobile product table  --> */}
          <section className="container mx-auto my-3 flex w-full flex-col gap-3 md:hidden">
            {/* <!-- 1 --> */}

            {data.map((item: any, index: any) => (
              <div className="flex w-full border px-4 py-4" key={index}>
                <img
                  className="self-start object-contain"
                  width="90px"
                  src="/assets/images/bedroom.png"
                  alt="bedroom image"
                />
                <div className="ml-3 flex w-full flex-col justify-center">
                  <div className="flex items-center justify-between">
                    <p className="text-xl font-bold">{item.name}</p>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                      className="h-5 w-5"
                    >
                      <path d="M9.653 16.915l-.005-.003-.019-.01a20.759 20.759 0 01-1.162-.682 22.045 22.045 0 01-2.582-1.9C4.045 12.733 2 10.352 2 7.5a4.5 4.5 0 018-2.828A4.5 4.5 0 0118 7.5c0 2.852-2.044 5.233-3.885 6.82a22.049 22.049 0 01-3.744 2.582l-.019.01-.005.003h-.002a.739.739 0 01-.69.001l-.002-.001z" />
                    </svg>
                  </div>
                  <p className="text-sm text-gray-400">Size: XL</p>
                  <p className="py-3 text-xl font-bold text-violet-900">$320</p>
                  <div className="mt-2 flex w-full items-center justify-between">
                    <div className="flex items-center justify-center">
                      <div className="flex cursor-text items-center justify-center active:ring-gray-500">
                        Quantity: 1
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </section>
          {/* <!-- /Mobile product table  --> */}

          {/* <!-- Product table  --> */}

          <table className="mt-3 hidden w-full lg:table">
            <thead className="h-16 bg-neutral-100">
              <tr>
                <th>ITEM</th>
                <th>HARGA</th>
                <th>QUANTITY</th>
                <th>TOTAL</th>
              </tr>
            </thead>
            <tbody>
              {/* <!-- 1 --> */}
              {data.map((item: any, index: any) => (
                <tr className="h-[100px] border-b" key={index}>
                  <td className="align-middle">
                    <div className="flex">
                      <img
                        className="w-[90px]"
                        src="/assets/images/bedroom.png"
                        alt="bedroom image"
                      />
                      <div className="ml-3 flex flex-col justify-center">
                        <p className="text-xl font-bold">{item.name}</p>
                        <p className="text-sm text-gray-400">
                          QTY Per Roll: {item.qtyPerRoll}
                        </p>
                      </div>
                    </div>
                  </td>
                  <td className="mx-auto text-center">IDR {item.price}</td>

                  <td className="text-center align-middle">{item.quantity}</td>
                  <td className="mx-auto text-center">IDR {item.totalPrice}</td>
                </tr>
              ))}
            </tbody>
          </table>
          {/* <!-- /Product table  --> */}

          <div className="flex w-full items-center justify-between">
            <a
              href="catalog.html"
              className="hidden text-sm text-violet-900 lg:block"
            >
              &larr; Back to the shop
            </a>

            <div className="mx-auto flex justify-center gap-2 lg:mx-0">
              <a
                href="checkout-payment.html"
                className="bg-purple-900 px-4 py-2 text-white"
              >
                Sebelumnya
              </a>

              <button
                // href="checkout-confirmation.html"
                className="bg-amber-400 px-4 py-2"
                onClick={() => actionCheckout()}
              >
                Buat Pesanan
              </button>
            </div>
          </div>
        </section>
        {/* <!-- /form  --> */}

        {/* <!-- Summary  --> */}

        <section className="mx-auto w-full px-4 md:max-w-[400px]">
          <div className="">
            <div className="border py-5 px-4 shadow-md">
              <p className="font-bold">RINGKASAN BELANJA</p>

              <div className="flex justify-between border-b py-5">
                <p>Subtotal</p>
                <p>$1280</p>
              </div>

              <div className="flex justify-between border-b py-5">
                <p>Total Ongkos Kirim</p>
                <p>Free</p>
              </div>

              <div className="flex justify-between py-5">
                <p>Total Harga</p>
                <p>IDR. {totalPrice}</p>
              </div>
            </div>
          </div>
        </section>
      </section>

      {/* <!-- /Summary --> */}

      {/* <!-- Cons bages --> */}

      <section className="container mx-auto my-8 flex flex-col justify-center gap-3 lg:flex-row">
        {/* <!-- 1 --> */}

        <div className="mx-5 flex flex-row items-center justify-center border-2 border-yellow-400 py-4 px-5">
          <div className="">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              className="h-6 w-6 text-violet-900 lg:mr-2"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M8.25 18.75a1.5 1.5 0 01-3 0m3 0a1.5 1.5 0 00-3 0m3 0h6m-9 0H3.375a1.125 1.125 0 01-1.125-1.125V14.25m17.25 4.5a1.5 1.5 0 01-3 0m3 0a1.5 1.5 0 00-3 0m3 0h1.125c.621 0 1.129-.504 1.09-1.124a17.902 17.902 0 00-3.213-9.193 2.056 2.056 0 00-1.58-.86H14.25M16.5 18.75h-2.25m0-11.177v-.958c0-.568-.422-1.048-.987-1.106a48.554 48.554 0 00-10.026 0 1.106 1.106 0 00-.987 1.106v7.635m12-6.677v6.677m0 4.5v-4.5m0 0h-12"
              />
            </svg>
          </div>

          <div className="ml-6 flex flex-col justify-center">
            <h3 className="text-left text-xs font-bold lg:text-sm">
              Free Delivery
            </h3>
            <p className="text-light text-center text-xs lg:text-left lg:text-sm">
              Orders from $200
            </p>
          </div>
        </div>

        {/* <!-- 2 --> */}

        <div className="mx-5 flex flex-row items-center justify-center border-2 border-yellow-400 py-4 px-5">
          <div className="">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              className="h-6 w-6 text-violet-900 lg:mr-2"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M2.25 18.75a60.07 60.07 0 0115.797 2.101c.727.198 1.453-.342 1.453-1.096V18.75M3.75 4.5v.75A.75.75 0 013 6h-.75m0 0v-.375c0-.621.504-1.125 1.125-1.125H20.25M2.25 6v9m18-10.5v.75c0 .414.336.75.75.75h.75m-1.5-1.5h.375c.621 0 1.125.504 1.125 1.125v9.75c0 .621-.504 1.125-1.125 1.125h-.375m1.5-1.5H21a.75.75 0 00-.75.75v.75m0 0H3.75m0 0h-.375a1.125 1.125 0 01-1.125-1.125V15m1.5 1.5v-.75A.75.75 0 003 15h-.75M15 10.5a3 3 0 11-6 0 3 3 0 016 0zm3 0h.008v.008H18V10.5zm-12 0h.008v.008H6V10.5z"
              />
            </svg>
          </div>

          <div className="ml-6 flex flex-col justify-center">
            <h3 className="text-left text-xs font-bold lg:text-sm">
              Money returns
            </h3>
            <p className="text-light text-left text-xs lg:text-sm">
              30 Days guarantee
            </p>
          </div>
        </div>

        {/* <!-- 3 --> */}

        <div className="mx-5 flex flex-row items-center justify-center border-2 border-yellow-400 py-4 px-5">
          <div className="">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              className="h-6 w-6 text-violet-900 lg:mr-2"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M9.879 7.519c1.171-1.025 3.071-1.025 4.242 0 1.172 1.025 1.172 2.687 0 3.712-.203.179-.43.326-.67.442-.745.361-1.45.999-1.45 1.827v.75M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9 5.25h.008v.008H12v-.008z"
              />
            </svg>
          </div>

          <div className="ml-6 flex flex-col justify-center">
            <h3 className="text-left text-xs font-bold lg:text-sm">
              24/7 Supports
            </h3>
            <p className="text-light text-left text-xs lg:text-sm">
              Consumer support
            </p>
          </div>
        </div>
      </section>

      {/* <!-- /Cons bages  --> */}
    </div>
  );
};

export default TinjauPesanan;
