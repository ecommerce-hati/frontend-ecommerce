"use client";

import { authService } from "@/services";
import { useEffect, useState } from "react";

const Login = () => {
  const [linkLogin, setLinkLogin] = useState<string>("");
  const btnGoogle = async () => {
    try {
      const response = await authService.LoginGoogle();

      // console.log(response.data);
      setLinkLogin(response.data.url);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    btnGoogle();
  });
  return (
    <>
      {/* <!-- Login card  --> */}
      <section className="mx-auto flex-grow w-full mt-10 mb-10 max-w-[1200px] px-5">
        <div className="container mx-auto border px-5 py-5 shadow-sm md:w-1/2">
          <div className="">
            <p className="text-4xl font-bold">LOGIN</p>
            <p>Selamat Datang Kembali, customer!</p>
          </div>

          <form className="mt-6 flex flex-col">
            <label htmlFor="email">Alamat Email</label>
            <input
              className="mb-3 mt-3 border px-4 py-2"
              type="email"
              placeholder="youremail@domain.com"
            />

            <label htmlFor="email">Password</label>
            <input
              className="mt-3 border px-4 py-2"
              type="password"
              placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;"
            />
          </form>

          <div className="mt-4 flex justify-between">
            <form className="flex gap-2">
              <input type="checkbox" />
              <label htmlFor="checkbox">Remember me</label>
            </form>
            <a href="#" className="text-violet-900">
              Forgot password
            </a>
          </div>

          <button className="my-5 w-full bg-violet-900 py-2 text-white">
            LOGIN
          </button>

          <p className="text-center text-gray-500">ATAU LOGIN DENGAN</p>

          <div className="my-5 flex gap-2">
            {/* <button className="w-1/2 bg-blue-800 py-2 text-white">
              FACEBOOK
            </button> */}
            <a
              href={linkLogin}
              className="w-full bg-orange-500 py-2 text-white text-center"
            >
              GOOGLE
            </a>
          </div>

          <p className="text-center">
            Tidak Punya Akun?
            <a href="sign-up.html" className="text-violet-900 ml-1">
              Daftar Sekarang
            </a>
          </p>
        </div>
      </section>
      {/* <!-- /Login Card  --> */}
    </>
  );
};

export default Login;
