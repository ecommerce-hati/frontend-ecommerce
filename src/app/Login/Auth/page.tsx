"use client";
import { useEffect, useState } from "react";
import { useRouter, useSearchParams } from "next/navigation"; // Menggunakan useRouter dari Next.js
import { usePathname } from "next/navigation";
import { useLocation } from "react-router-dom";

const Auth = () => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState({});
  // const router = useRouter(); // Menggunakan useRouter
  // const location = useLocation(); // Menggunakan useRouter
  // const queryString = window.location.search;

  // const urlParams = new URLSearchParams(queryString);
  // const paramValue = urlParams.get('paramName');
  // const searchParams = useSearchParams();
  // const search = searchParams.get("code");
  const searchParams = window.location.search;

  console.log(searchParams);

  // const pathname = usePathname();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(
          // `http://192.168.0.148:8000/api/auth/callback${searchParams}`,
          `http://192.168.8.56:8000/api/auth/callback${searchParams}`,
          {
            headers: {
              "Content-Type": "application/json",
              Accept: "application/json",
            },
          }
        );

        if (!response.ok) {
          throw new Error("Error fetching data");
        }

        const data = await response.json();
        console.log(data);
        setData(data);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching data:", error);
        setLoading(false);
      }
    };

    fetchData();
  }, []); // Gunakan router.asPath sebagai dependensi useEffect

  return null; // Pastikan return null jika tidak ada yang dirender dalam komponen
};

export default Auth;
