/* eslint-disable @next/next/no-img-element */
"use client";

import Image from "next/image";
import Link from "next/link";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { IoShirtOutline } from "react-icons/io5";
import { FaEye } from "react-icons/fa";
import { productService } from "@/services";
import { useEffect, useState } from "react";
import { ListProduct } from "@/types";
import { ConvertNamaProduk } from "./Utils";

function SampleNextArrow(props: any) {
  const { className, style, onClick } = props;

  return (
    <button
      className={className}
      style={{
        ...style,
        display: "block",
        // background: "#989696",
        color: "red",
      }}
      onClick={onClick}
    />
  );
}
function SamplePrevArrow(props: any) {
  const { className, style, onClick } = props;
  return (
    <button
      className={className}
      style={{
        ...style,
        display: "block",
        background: "#fbbf25",
        borderRadius: "100%",
        color: "red",
      }}
      onClick={onClick}
    >
      tes
    </button>
  );
}

const Home = () => {
  const [listProdukTerlaris, setListProdukTerlaris] =
    useState<Array<ListProduct>>();

  const [listProdukPalingBanyakDilihat, setListProdukPalingBanyakDilihat] =
    useState<Array<ListProduct>>();

  const [listProdukTerbaru, setListProdukTerbaru] =
    useState<Array<ListProduct>>();

  const getProductTerlaris = async () => {
    try {
      const response = await productService.getListProductTop(1, 8);

      // console.log(response.data.data);
      setListProdukTerlaris(response?.data?.data);
    } catch (error) {}
  };

  const getProductPalingBanyakDilihat = async () => {
    try {
      const response = await productService.getListProductTop(2, 8);

      // console.log(response.data.data);
      setListProdukPalingBanyakDilihat(response?.data?.data);
    } catch (error) {}
  };

  const getProductTerbaru = async () => {
    try {
      const response = await productService.getListProductTop(3, 8);

      // console.log(response.data.data);
      setListProdukTerbaru(response?.data?.data);
    } catch (error) {}
  };
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    arrows: true,
    initialSlide: 0,
    nextArrow: <SamplePrevArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
    ],
  };

  useEffect(() => {
    getProductTerlaris();
    getProductPalingBanyakDilihat();
    getProductTerbaru();
  }, [listProdukPalingBanyakDilihat, listProdukTerbaru, listProdukTerlaris]);
  return (
    <>
      <div className="relative">
        <img
          className="w-full object-cover brightness-50 filter lg:h-[500px]"
          src="/assets/images/header-bg.jpeg"
          alt="Living room image"
        />

        <div className="absolute top-1/2 left-1/2 mx-auto flex w-11/12 max-w-[1200px] -translate-x-1/2 -translate-y-1/2 flex-col text-center text-white lg:ml-5">
          <h1 className="text-4xl font-bold sm:text-5xl lg:text-left">
            Best Collection for Home decoration
          </h1>
          <p className="pt-3 text-xs lg:w-3/5 lg:pt-5 lg:text-left lg:text-base">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit.
            Consequatur aperiam natus, nulla, obcaecati nesciunt, itaque
            adipisci earum ducimus pariatur eaque labore.
          </p>
          <button className="mx-auto mt-5 w-1/2 bg-amber-400 px-3 py-1 text-black duration-100 hover:bg-yellow-300 lg:mx-0 lg:h-10 lg:w-2/12 lg:px-10">
            Beli Sekarang
          </button>
        </div>
      </div>

      <section className="container mx-auto my-8 flex flex-col justify-center gap-3 lg:flex-row">
        <div className="mx-5 flex flex-row items-center justify-center border-2 border-yellow-400 py-4 px-5">
          <div className="">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="h-6 w-6 text-violet-900 lg:mr-2"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M8.25 18.75a1.5 1.5 0 01-3 0m3 0a1.5 1.5 0 00-3 0m3 0h6m-9 0H3.375a1.125 1.125 0 01-1.125-1.125V14.25m17.25 4.5a1.5 1.5 0 01-3 0m3 0a1.5 1.5 0 00-3 0m3 0h1.125c.621 0 1.129-.504 1.09-1.124a17.902 17.902 0 00-3.213-9.193 2.056 2.056 0 00-1.58-.86H14.25M16.5 18.75h-2.25m0-11.177v-.958c0-.568-.422-1.048-.987-1.106a48.554 48.554 0 00-10.026 0 1.106 1.106 0 00-.987 1.106v7.635m12-6.677v6.677m0 4.5v-4.5m0 0h-12"
              />
            </svg>
          </div>

          <div className="ml-6 flex flex-col justify-center">
            <h3 className="text-left text-xs font-bold lg:text-sm">
              Free Delivery
            </h3>
            <p className="text-light text-center text-xs lg:text-left lg:text-sm">
              Orders from $200
            </p>
          </div>
        </div>

        <div className="mx-5 flex flex-row items-center justify-center border-2 border-yellow-400 py-4 px-5">
          <div className="">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="h-6 w-6 text-violet-900 lg:mr-2"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M2.25 18.75a60.07 60.07 0 0115.797 2.101c.727.198 1.453-.342 1.453-1.096V18.75M3.75 4.5v.75A.75.75 0 013 6h-.75m0 0v-.375c0-.621.504-1.125 1.125-1.125H20.25M2.25 6v9m18-10.5v.75c0 .414.336.75.75.75h.75m-1.5-1.5h.375c.621 0 1.125.504 1.125 1.125v9.75c0 .621-.504 1.125-1.125 1.125h-.375m1.5-1.5H21a.75.75 0 00-.75.75v.75m0 0H3.75m0 0h-.375a1.125 1.125 0 01-1.125-1.125V15m1.5 1.5v-.75A.75.75 0 003 15h-.75M15 10.5a3 3 0 11-6 0 3 3 0 016 0zm3 0h.008v.008H18V10.5zm-12 0h.008v.008H6V10.5z"
              />
            </svg>
          </div>

          <div className="ml-6 flex flex-col justify-center">
            <h3 className="text-left text-xs font-bold lg:text-sm">
              Money returns
            </h3>
            <p className="text-light text-left text-xs lg:text-sm">
              30 Days guarantee
            </p>
          </div>
        </div>

        <div className="mx-5 flex flex-row items-center justify-center border-2 border-yellow-400 py-4 px-5">
          <div className="">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="h-6 w-6 text-violet-900 lg:mr-2"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M9.879 7.519c1.171-1.025 3.071-1.025 4.242 0 1.172 1.025 1.172 2.687 0 3.712-.203.179-.43.326-.67.442-.745.361-1.45.999-1.45 1.827v.75M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9 5.25h.008v.008H12v-.008z"
              />
            </svg>
          </div>

          <div className="ml-6 flex flex-col justify-center">
            <h3 className="text-left text-xs font-bold lg:text-sm">
              24/7 Supports
            </h3>
            <p className="text-light text-left text-xs lg:text-sm">
              Consumer support
            </p>
          </div>
        </div>
      </section>

      <p className="mx-auto mt-10 mb-5 max-w-[1200px] px-5 uppercase">
        Produk Terlaris
      </p>

      {/* <!-- Slider  --> */}
      <div className="slider-container mx-auto w-[80vw] md:max-w-[1200px] ">
        <Slider {...settings}>
          {listProdukTerlaris?.map((produkTerlaris: any, index: any) => (
            <div className="flex flex-col sm:h-auto sm:max-h-[1200px]  justify-center  px-2">
              <div className="border border-gray-300 pb-4 rounded-xl">
                <div className="h-2/4 ">
                  <a href="Detail-Produk" className="">
                    <div className="relative flex">
                      <div className="h-full md:h-full ">
                        <Image
                          src={"/assets/images/product-chair.png"}
                          width={300}
                          height={0}
                          className="h-full rounded-t-xl"
                          alt=""
                        />
                      </div>
                      <div className="absolute flex h-full w-full items-center justify-center gap-3 opacity-0 duration-150 hover:opacity-100">
                        <span className="flex h-8 w-8 cursor-pointer items-center justify-center rounded-full bg-amber-400">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth="1.5"
                            stroke="currentColor"
                            className="h-4 w-4"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                            />
                          </svg>
                        </span>
                      </div>
                    </div>

                    <div>
                      <div className="bg-gradient-to-r from-violet-900 to-purple-500  py-1 px-2 rounded-br-full flex justify-between items-center">
                        <p className="uppercase text-white font-medium text-sm md:text-base ">
                          <IoShirtOutline />
                        </p>
                        <p className="uppercase text-white font-medium text-xs md:text-sm px-4">
                          #{produkTerlaris.jenis_kain}
                        </p>
                      </div>
                      <div className="px-2">
                        <div className="mt-2 flex justify-between gap-2 ">
                          <p className="uppercase text-sm md:text-base w-2/4 md:w-3/4 ">
                            {ConvertNamaProduk(produkTerlaris.nama_design)}
                          </p>
                        </div>
                        <div className="flex justify-between my-1">
                          <p className="text-xs md:text-sm">
                            {produkTerlaris.warna_varian} Varian Warna |{" "}
                            {produkTerlaris.grade_varian} Varian Grade
                          </p>
                        </div>
                        <p className=" font-semibold  text-sm md:text-base  text-violet-900 my-2">
                          IDR {produkTerlaris.harga} / Yard
                        </p>

                        <p className="text-xs md:text-sm text-gray-500 flex items-center">
                          <FaEye className="mr-2 text-base" />{" "}
                          {produkTerlaris.qty_view} Dilihat |{" "}
                          {produkTerlaris.qty_order} Terjual
                        </p>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          ))}
        </Slider>
      </div>

      <p className="mx-auto mt-10 mb-5 max-w-[1200px] px-5 uppercase">
        Produk Paling Banyak Dilihat
      </p>

      {/* <!-- Slider  --> */}
      <div className="slider-container mx-auto w-[80vw] md:max-w-[1200px] ">
        <Slider {...settings}>
          {listProdukPalingBanyakDilihat?.map(
            (produkPalingBanyakDilihat: any, index: any) => (
              <div className="flex flex-col sm:h-auto sm:max-h-[1200px] md:h-[50vh]  justify-center  px-2 ">
                <div className="border border-gray-300 pb-4 rounded-xl h-full">
                  <div className="h-2/4 ">
                    <a href="Detail-Produk" className="">
                      <div className="relative flex">
                        <div className="h-full md:h-full ">
                          <Image
                            src={"/assets/images/product-chair.png"}
                            width={300}
                            height={0}
                            className="h-full rounded-t-xl"
                            alt=""
                          />
                        </div>
                        <div className="absolute flex h-full w-full items-center justify-center gap-3 opacity-0 duration-150 hover:opacity-100">
                          <span className="flex h-8 w-8 cursor-pointer items-center justify-center rounded-full bg-amber-400">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              fill="none"
                              viewBox="0 0 24 24"
                              strokeWidth="1.5"
                              stroke="currentColor"
                              className="h-4 w-4"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                              />
                            </svg>
                          </span>
                        </div>
                      </div>

                      <div>
                        <div className="bg-gradient-to-r from-violet-900 to-purple-500  py-1 px-2 rounded-br-full flex justify-between items-center">
                          <p className="uppercase text-white font-medium text-sm md:text-base ">
                            <IoShirtOutline />
                          </p>
                          <p className="uppercase text-white font-medium text-xs md:text-sm px-4">
                            #{produkPalingBanyakDilihat.jenis_kain}
                          </p>
                        </div>
                        <div className="px-2">
                          <div className="mt-2 flex justify-between gap-2 ">
                            <p className="uppercase text-sm md:text-base w-2/4 md:w-3/4 ">
                              {ConvertNamaProduk(
                                produkPalingBanyakDilihat.nama_design
                              )}
                            </p>
                          </div>
                          <div className="flex justify-between my-1">
                            <p className="text-xs md:text-sm">
                              {produkPalingBanyakDilihat.warna_varian} Varian
                              Warna | {produkPalingBanyakDilihat.grade_varian}{" "}
                              Varian Grade
                            </p>
                          </div>
                          <p className=" font-semibold  text-sm md:text-base  text-violet-900 my-2">
                            IDR {produkPalingBanyakDilihat.harga} / Yard
                          </p>

                          <p className="text-xs md:text-sm text-gray-500 flex items-center">
                            <FaEye className="mr-2 text-base" />{" "}
                            {produkPalingBanyakDilihat.qty_view} Dilihat |{" "}
                            {produkPalingBanyakDilihat.qty_order} Terjual
                          </p>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            )
          )}
        </Slider>
      </div>

      <h2 className="mx-auto mt-10 mb-5 max-w-[1200px] px-5 uppercase">
        Belanja berdasarkan Kategori
      </h2>

      {/* <!-- Cathegories --> */}
      <section className="mx-auto grid max-w-[1200px]  grid-cols-2 lg:gap-5 ">
        <a href="/List-Produk/Fashion" className="w-full">
          <div className="relative cursor-pointer">
            <img
              className="mx-auto h-auto w-auto brightness-50 duration-300 hover:brightness-100"
              src="./assets/images/matrass.png"
              alt="Matrass cathegory image"
            />

            <p className="pointer-events-none absolute top-1/2 left-1/2 w-11/12 -translate-x-1/2 -translate-y-1/2 text-center text-white lg:text-xl">
              Fashion
            </p>
          </div>
        </a>

        <a href="/List-Produk/Sprei">
          <div className="relative cursor-pointer">
            <img
              className="mx-auto h-auto w-auto brightness-50 duration-300 hover:brightness-100"
              src="./assets/images/matrass.png"
              alt="Matrass cathegory image"
            />

            <p className="pointer-events-none absolute top-1/2 left-1/2 w-11/12 -translate-x-1/2 -translate-y-1/2 text-center text-white lg:text-xl">
              Sprei
            </p>
          </div>
        </a>
      </section>
      {/* <!-- /Cathegories  --> */}

      <p className="mx-auto mt-10 mb-5 max-w-[1200px] px-5 uppercase">
        Produk Terbaru
      </p>

      {/* <!-- Slider  --> */}
      <div className="slider-container mx-auto w-[80vw] md:max-w-[1200px] ">
        <Slider {...settings}>
          {listProdukTerbaru?.map((produkTerbaru: any, index: any) => (
            <div className="flex flex-col sm:h-auto sm:max-h-[1200px]  justify-center  px-2">
              <div className="border border-gray-300 pb-4 rounded-xl">
                <div className="h-2/4 ">
                  <a href="Detail-Produk" className="">
                    <div className="relative flex">
                      <div className="h-full md:h-full ">
                        <Image
                          src={"/assets/images/product-chair.png"}
                          width={300}
                          height={0}
                          className="h-full rounded-t-xl"
                          alt=""
                        />
                      </div>
                      <div className="absolute flex h-full w-full items-center justify-center gap-3 opacity-0 duration-150 hover:opacity-100">
                        <span className="flex h-8 w-8 cursor-pointer items-center justify-center rounded-full bg-amber-400">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth="1.5"
                            stroke="currentColor"
                            className="h-4 w-4"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                            />
                          </svg>
                        </span>
                      </div>
                    </div>

                    <div>
                      <div className="bg-gradient-to-r from-violet-900 to-purple-500  py-1 px-2 rounded-br-full flex justify-between items-center">
                        <p className="uppercase text-white font-medium text-sm md:text-base ">
                          <IoShirtOutline />
                        </p>
                        <p className="uppercase text-white font-medium text-xs md:text-sm px-4">
                          #{produkTerbaru.jenis_kain}
                        </p>
                      </div>
                      <div className="px-2">
                        <div className="mt-2 flex justify-between gap-2 ">
                          <p className="uppercase text-sm md:text-base w-2/4 md:w-3/4 ">
                            {ConvertNamaProduk(produkTerbaru.nama_design)}
                          </p>
                        </div>
                        <div className="flex justify-between my-1">
                          <p className="text-xs md:text-sm">
                            {produkTerbaru.warna_varian} Varian Warna |{" "}
                            {produkTerbaru.grade_varian} Varian Grade
                          </p>
                        </div>
                        <p className=" font-semibold  text-sm md:text-base  text-violet-900 my-2">
                          IDR {produkTerbaru.harga} / Yard
                        </p>

                        <p className="text-xs md:text-sm text-gray-500 flex items-center">
                          <FaEye className="mr-2 text-base" />{" "}
                          {produkTerbaru.qty_view} Dilihat |{" "}
                          {produkTerbaru.qty_order} Terjual
                        </p>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          ))}
        </Slider>
      </div>

      {/* promo */}
      <div className="mx-auto max-w-[1200px] px-5">
        <section className="mt-10 flex max-w-[1200px] justify-between bg-violet-900 px-5">
          <div className="py-8 px-3 lg:px-16">
            <p className="text-white">ONLINE EXCLUSIVE</p>
            <h2 className="pt-6 text-5xl font-bold text-yellow-400">15% OFF</h2>
            <p className="pt-4 text-white">
              ACCENT CHAIRS, <br />
              TABLES & OTTOMANS
            </p>
            <button
              // href="#"
              className="mt-6 bg-amber-400 px-4 py-2 duration-100 hover:bg-yellow-300"
            >
              Shop now
            </button>
          </div>

          <img
            className="-mr-5 hidden w-[550px] object-cover md:block"
            src="./assets/images/sale-bage.jpeg"
            alt="Rainbow credit card with macbook on a background"
          />
        </section>
      </div>
    </>
  );
};

export default Home;
