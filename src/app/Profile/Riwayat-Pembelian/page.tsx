/* eslint-disable @next/next/no-img-element */
"use client";

import Image from "next/image";
import { useState } from "react";
import {
  MdOutlineKeyboardArrowDown,
  MdOutlineKeyboardArrowUp,
} from "react-icons/md";

const RiwayatPembelian = () => {
  const [accordionActive, setAccordionActive] = useState<Array<boolean>>([
    false,
    false,
  ]); // Gunakan array untuk menyimpan status masing-masing accordion

  const toggleAccordion = (index: number) => {
    const newAccordionActive = [...accordionActive]; // Buat salinan array agar imutabilitas terjaga
    newAccordionActive[index] = !newAccordionActive[index]; // Ubah status accordion yang dipilih
    setAccordionActive(newAccordionActive); // Update status accordion
  };

  return (
    <>
      <section className="container flex-grow mx-auto max-w-[1200px] border-b py-5 lg:flex lg:flex-row lg:py-10">
        {/* <!-- sidebar  --> */}
        <section className="hidden w-[300px] flex-shrink-0 px-4 lg:block">
          <div className="border-b py-5">
            <div className="flex items-center">
              <img
                width="40px"
                height="40px"
                className="rounded-full object-cover"
                src="/assets/images/avatar-photo.png"
                alt="Red woman portrait"
              />
              <div className="ml-5">
                <p className="font-medium text-gray-500">Hello,</p>
                <p className="font-bold">Sarah Johnson</p>
              </div>
            </div>
          </div>

          <div className="flex border-b py-5">
            <div className="w-full">
              <div className="flex w-full">
                <div className="flex flex-col gap-2">
                  <a
                    href="account-page.html"
                    className="flex items-center gap-2 font-medium"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      className="h-5 w-5"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M15 9h3.75M15 12h3.75M15 15h3.75M4.5 19.5h15a2.25 2.25 0 002.25-2.25V6.75A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25v10.5A2.25 2.25 0 004.5 19.5zm6-10.125a1.875 1.875 0 11-3.75 0 1.875 1.875 0 013.75 0zm1.294 6.336a6.721 6.721 0 01-3.17.789 6.721 6.721 0 01-3.168-.789 3.376 3.376 0 016.338 0z"
                      />
                    </svg>
                    Kelola Akun
                  </a>
                  <a href="/Profile" className="text-violet-900 duration-100">
                    Akun Saya
                  </a>
                  <a
                    href="/Profile/Alamat-Profil"
                    className="text-gray-500 duration-100 hover:text-yellow-400"
                  >
                    Kelola Alamat
                  </a>
                  <a
                    href="change-password.html"
                    className="text-gray-500 duration-100 hover:text-yellow-400"
                  >
                    Ubah Kata Sandi
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div className="flex border-b py-5">
            <div className="flex w-full">
              <div className="flex flex-col gap-2">
                <a
                  href="/Profile/Riwayat-Pembelian"
                  className="flex items-center gap-2 font-medium active:text-violet-900"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 24 24"
                    fill="currentColor"
                    className="h-5 w-5"
                  >
                    <path d="M3.375 3C2.339 3 1.5 3.84 1.5 4.875v.75c0 1.036.84 1.875 1.875 1.875h17.25c1.035 0 1.875-.84 1.875-1.875v-.75C22.5 3.839 21.66 3 20.625 3H3.375z" />
                    <path
                      fill-rule="evenodd"
                      d="M3.087 9l.54 9.176A3 3 0 006.62 21h10.757a3 3 0 002.995-2.824L20.913 9H3.087zm6.163 3.75A.75.75 0 0110 12h4a.75.75 0 010 1.5h-4a.75.75 0 01-.75-.75z"
                      clip-rule="evenodd"
                    />
                  </svg>
                  Pesanan Saya
                </a>
              </div>
            </div>
          </div>

          <div className="flex border-b py-5">
            <div className="flex w-full">
              <div className="flex flex-col gap-2">
                <a
                  href="payment-methods.html"
                  className="flex items-center gap-2 font-medium active:text-violet-900"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke-width="1.5"
                    stroke="currentColor"
                    className="h-5 w-5"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M2.25 8.25h19.5M2.25 9h19.5m-16.5 5.25h6m-6 2.25h3m-3.75 3h15a2.25 2.25 0 002.25-2.25V6.75A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25v10.5A2.25 2.25 0 004.5 19.5z"
                    />
                  </svg>
                  Metode Pembayaran
                </a>
              </div>
            </div>
          </div>

          <div className="flex py-5">
            <div className="flex w-full">
              <div className="flex flex-col gap-2">
                <a
                  href="#"
                  className="flex items-center gap-2 font-medium active:text-violet-900"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke-width="1.5"
                    stroke="currentColor"
                    className="h-5 w-5"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9"
                    />
                  </svg>
                  Keluar
                </a>
              </div>
            </div>
          </div>
        </section>
        {/* <!-- /sidebar  --> */}

        {/* <!-- Order table  --> */}
        <section className="hidden h-full w-full max-w-[1200px] grid-cols-1 gap-3 px-5 pb-10 lg:grid">
          {accordionActive.map((isActive, index) => (
            <div key={index}>
              <div
                className="px-16 py-6 h-auto flex justify-between border border-gray-700 rounded-t-lg "
                onClick={() => toggleAccordion(index)} // Panggil fungsi toggleAccordion saat accordion diklik
              >
                <div className="flex gap-4">
                  <div>
                    <div>ID : 123456789</div>
                    <div>Tanggal : 15 Maret 2024</div>
                    <div>Status : Lorem Ipsum</div>
                  </div>
                </div>
                <div>
                  <div>Total : 123456789</div>
                  <div>Terbayar : 15 Maret 2024</div>
                  <div>Status kirim : Lorem Ipsum</div>
                </div>
                <button className="flex items-center text-2xl">
                  {isActive ? (
                    <MdOutlineKeyboardArrowUp />
                  ) : (
                    <MdOutlineKeyboardArrowDown />
                  )}
                </button>
              </div>
              <div
                className={`px-16 py-6 h-auto flex justify-between border-r border-l border-b border-gray-700 rounded-b-lg ${
                  isActive ? "" : "hidden"
                }`}
              >
                <section className="hidden h-auto w-full max-w-[1200px] grid-cols-1 gap-3 px-5 pb-10 md:grid">
                  <table className="table-fixed">
                    <thead className="h-16 bg-neutral-100">
                      <tr>
                        <th>ITEM</th>
                        <th>PRICE</th>
                        <th>QUANTITY</th>
                        <th>TOTAL</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      {/* <!-- 1 --> */}

                      <tr className="h-[100px] border-b">
                        <td className="align-middle">
                          <div className="flex">
                            <img
                              className="w-[90px]"
                              src="/assets/images/bedroom.png"
                              alt="bedroom image"
                            />
                            <div className="ml-3 flex flex-col justify-center">
                              <p className="text-xl font-bold">ITALIAN BED</p>
                              <p className="text-sm text-gray-400">
                                QTY Per Roll: 25
                              </p>
                            </div>
                          </div>
                        </td>
                        <td className="mx-auto text-center">IDR 15,000.00</td>
                        <td className="align-middle">
                          <div className="flex items-center justify-center">
                            <div className="flex h-8 w-8 cursor-text items-center justify-center">
                              1
                            </div>
                          </div>
                        </td>
                        <td className="mx-auto text-center">IDR 15,000.00</td>
                      </tr>

                      {/* <!-- 2 --> */}

                      <tr className="h-[100px] border-b">
                        <td className="align-middle">
                          <div className="flex">
                            <img
                              className="w-[90px]"
                              src="/assets/images/bedroom.png"
                              alt="bedroom image"
                            />
                            <div className="ml-3 flex flex-col justify-center">
                              <p className="text-xl font-bold">ITALIAN BED</p>
                              <p className="text-sm text-gray-400">
                                QTY Per Roll: 25
                              </p>
                            </div>
                          </div>
                        </td>
                        <td className="mx-auto text-center">IDR 15,000.00</td>
                        <td className="align-middle">
                          <div className="flex items-center justify-center">
                            <div className="flex h-8 w-8 cursor-text items-center justify-center">
                              1
                            </div>
                          </div>
                        </td>
                        <td className="mx-auto text-center">IDR 15,000.00</td>
                      </tr>
                    </tbody>
                  </table>
                </section>
              </div>
            </div>
          ))}
        </section>
        {/* <!-- /Order table  --> */}
      </section>
    </>
  );
};

export default RiwayatPembelian;
